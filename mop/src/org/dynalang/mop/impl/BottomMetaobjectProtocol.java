package org.dynalang.mop.impl;

import java.util.Iterator;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Map.Entry;

import org.dynalang.mop.CallProtocol;

/**
 * An implementation for "bottom" metaobject protocol, one that returns 
 * authoritative answer for all methods, and claims nonexistence (and 
 * non-callability) for everything. Useful as a last element in a composite 
 * metaobject protocol if the user of the composite metaobject protocol is not 
 * able or not willing to deal with non-authoritative answers.  
 * @author Attila Szegedi
 * @version $Id: $
 */
public class BottomMetaobjectProtocol extends MetaobjectProtocolBase {
    public Object call(Object callable, CallProtocol callProtocol, Map args) {
        return Result.notCallable;
    }
    
    public Object call(Object callable, CallProtocol callProtocol, Object... args) {
        return Result.notCallable;
    }
    
    public Result delete(Object target, Object propertyId) {
        return Result.doesNotExist;
    }
    
    public Object get(Object target, Object propertyId) {
        return Result.doesNotExist;
    }
    
    public Boolean has(Object target, Object propertyId) {
        return Boolean.FALSE;
    }
    
    public Iterator<Map.Entry> properties(Object target) {
	return new Iterator<Map.Entry>() {
	    public boolean hasNext() {
	        return false;
	    }
	    public Entry next() {
	        throw new NoSuchElementException(); 
	    }
	    public void remove() {
	        throw new NoSuchElementException(); 
	    }
	};
    }
    
    public Result put(Object target, Object propertyId, Object value, CallProtocol callProtocol) {
	return Result.doesNotExist;
    }
    
    public Object representAs(Object object, Class targetClass) {
	return Result.noRepresentation;
    }
}
