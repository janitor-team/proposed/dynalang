/*
   Copyright 2007 Attila Szegedi

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package org.dynalang.mop.beans;

import java.lang.reflect.Member;
import java.lang.reflect.UndeclaredThrowableException;

import org.dynalang.classtoken.ClassToken;
import org.dynalang.mop.CallProtocol;

/**
 * TODO: rename "unwrap"
 * @author Attila Szegedi
 * @version $Id: $
 * @param <T>
 */
class OverloadedDynamicMethod<T extends Member> extends DynamicMethod<T>
{
    static final Object NO_SUCH_METHOD = new Object();
    static final Object AMBIGUOUS_METHOD = new Object();
    
    private final OverloadedFixArgMethod<T> fixArgMethod = new OverloadedFixArgMethod<T>();
    private final String name;
    private final Class<?> declaringClass;
    private OverloadedVarArgMethod<T> varArgMethod;
    
    OverloadedDynamicMethod(String name, Class<?> declaringClass) {
        this.name = name;
        this.declaringClass = declaringClass;
    }
    
    void onClassTokensInvalidated(ClassToken[] tokens) {
	fixArgMethod.onClassTokensInvalidated(tokens);
	if(varArgMethod != null) {
	    varArgMethod.onClassTokensInvalidated(tokens);
	}
    }
    
    @Override
    public Object call(Object target, CallProtocol callProtocol, Object... args) {
        // Try fixarg first
        Object invocation = fixArgMethod.createInvocation(target, args, callProtocol);
        if(invocation == NO_SUCH_METHOD) {
            if(varArgMethod != null) {
                invocation = varArgMethod.createInvocation(target, args, callProtocol);
            }
            if(invocation == NO_SUCH_METHOD) {
                throw new IllegalArgumentException("No signature of method " + 
                        name + " on " + declaringClass + " matches the arguments");
            }
        }
        if(invocation == AMBIGUOUS_METHOD) {
            throw new IllegalArgumentException("Multiple signatures of method " + 
                    name + " on " + declaringClass + " match the arguments");
        }
        try {
            return ((Invocation<?>)invocation).invoke();
        }
        catch(RuntimeException e) {
            throw e;
        }
        catch(Exception e) {
            throw new UndeclaredThrowableException(e);
        }
    }

    void addMember(T member) {
        fixArgMethod.addMember(member);
        boolean isVarArg = isVarArgs(member);
        if(isVarArg) {
            if(varArgMethod == null) {
                varArgMethod = new OverloadedVarArgMethod<T>();
            }
            varArgMethod.addMember(member);
        }
    }
}