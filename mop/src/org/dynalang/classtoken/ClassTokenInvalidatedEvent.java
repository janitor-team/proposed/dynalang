package org.dynalang.classtoken;

import java.util.EventObject;

/**
 * Event that is fired sometime (no later than next invocation of 
 * {@link ClassToken#forClass(Class)})after one or more class tokens becomes 
 * invalid (the classes they represent became less than weakly reachable).
 * @author Attila Szegedi
 * @version $Id: $
 */
public class ClassTokenInvalidatedEvent extends EventObject {
    private static final long serialVersionUID = 1L;

    ClassTokenInvalidatedEvent(ClassToken[] source) {
	super(source);
    }
    
    /**
     * Returns the class token objects that were invalidated
     * @return the invalidated class tokens
     */
    public ClassToken[] getClassTokens() {
	return (ClassToken[])getSource();
    }
}